'use strict';

// Declare app level module which depends on views, and components
angular.module('tidy', [
  'ngRoute',
  'tidy.calculator'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/'});
}]);
