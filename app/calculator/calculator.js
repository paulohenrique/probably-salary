'use strict';

angular.module('tidy.calculator', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/', {
    templateUrl: 'calculator/calculator.html',
    controller: 'CalculatorCtrl'
  });
}])

.factory('calculatorService', ['$http', function($http) {
  var getData = function() {
    return $http({method:'GET', url:"https://openexchangerates.org/api/latest.json?app_id=f55fc2c224de4da4a5df6f75325bbd1f"}).then(function(result){
      return result.data;
    });
  };
  return { getData: getData };
}])

.controller('CalculatorCtrl', ['$rootScope', '$http', 'calculatorService', function($scope, $http, calculatorService) {
  $scope.expect = {};
  var service = calculatorService.getData();
  service.then(function(result) {
    $scope.rates = result.rates;
    if ( typeof fx != "undefined") {
      fx.rates = result.rates;
      fx.base = result.base;
    } else {
      var fxSetup = {
        rates : result.rates,
        base : result.base
      }
    }
  });

  $scope.showResult = function (expect){
    var probably_salary = (expect.salary * expect.hours) * expect.days * 4;
    var total = fx.convert(probably_salary, {from: expect.company, to: expect.rate});
    $scope.result = total;
  };
}]);