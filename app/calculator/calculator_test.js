'use strict';

describe('tidy.calculator module', function() {
  var scope, calculatorService, $location;
  beforeEach(module('tidy.calculator'));

  beforeEach(inject(function($controller, $rootScope, _$location_, _calculatorService) {
    scope = $rootScope.$new();
    $location = _$location_;
    calculatorService = _calculatorService_;
    $controller('CalculatorCtrl',
                {$scope: scope, $location: $location, calculatorService: calculatorService });

    scope.$digest();

  }));

  beforeEach(function() {
    var mockCalculatorService = {};
    module('tidy', function($provide) {
      $provide.value('calculatorService', mockCalculatorService);
    });

    inject(function($q) {
      mockCalculatorService.data = {
        "disclaimer": "Exchange rates...",
        "license": "Data sourced from...",
        "timestamp": 1418738459,
        "base": "USD",
        "rates": {
          "BRL": 2.714742,
          "SEK": 7.610042,
          "SGD": 1.303854,
          "SHP": 0.635042,
          "SLL": 4286.666667,
          "SOS": 717.859398,
          "SRD": 3.2875,
          "STD": 19579.483333,
          "USD": 1,
        }
      };

      mockCalculatorService.getData = function() {
        var defer = $q.defer();

        defer.resolve(this.data);

        return defer.promise;
      };
    });
  });


  describe('calculator controller', function(){

    it('should be defined', inject(function($controller) {
      var calculatorCtrl = $controller('CalculatorCtrl');
      expect(calculatorCtrl).toBeDefined();
    }));

    it('should contain all the rates at startup', function() {
      expect(scope.rates).toEqual({
        "BRL": 2.714742,
        "SEK": 7.610042,
        "SGD": 1.303854,
        "SHP": 0.635042,
        "SLL": 4286.666667,
        "SOS": 717.859398,
        "SRD": 3.2875,
        "STD": 19579.483333,
        "USD": 1
      });
    });

  });
});