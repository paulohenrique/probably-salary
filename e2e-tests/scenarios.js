'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('Tidy app', function() {

  browser.get('index.html');

  it('should automatically redirect to / when location hash/fragment is empty', function() {
    expect(browser.getLocationAbsUrl()).toMatch("/");
  });


  describe('calculator', function() {

    beforeEach(function() {
      browser.get('index.html#/');
    });


    it('should render calculator when user navigates to /', function() {
      expect(element.all(by.css('[ng-view] h1')).first().getText()).
        toMatch(/Calcule your monthly salary/);
    });

  });

});
